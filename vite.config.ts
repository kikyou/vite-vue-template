import Vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import AutoImport from 'unplugin-auto-import/vite';
import IconsResolver from 'unplugin-icons/resolver';
import Icons from 'unplugin-icons/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import Components from 'unplugin-vue-components/vite';
import { defineConfig } from 'vite';

const pathSrc = resolve(__dirname, 'src');

export default defineConfig({
  build: {
    cssCodeSplit: false,
  },
  resolve: {
    alias: {
      '@': pathSrc,
      '@api': `${pathSrc}/api`,
      '@config': `${pathSrc}/config`,
      '@hooks': `${pathSrc}/hooks`,
      '@components': `${pathSrc}/components`,
      '@views': `${pathSrc}/views`,
      '@router': `${pathSrc}/router`,
      '@store': `${pathSrc}/store`,
      '@style': `${pathSrc}/style`,
      '@utils': `${pathSrc}/utils`,
    },
  },
  plugins: [
    Vue({
      reactivityTransform: true,
    }),
    AutoImport({
      // Auto import functions from Vue, e.g. ref, reactive, toRef...
      // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
      imports: [
        'vue',
        {
          'vue-router': [
            'useRouter',
            'useRoute',
            'onBeforeRouteLeave',
            'createRouter',
            'createWebHashHistory',
          ],
        },
        { pinia: ['createPinia', 'storeToRefs', 'defineStore'] },
      ],

      // Auto import functions from Element Plus, e.g. ElMessage, ElMessageBox... (with style)
      // 自动导入 Element Plus 相关函数，如：ElMessage, ElMessageBox... (带样式)
      resolvers: [
        ElementPlusResolver(),

        // Auto import icon components
        // 自动导入图标组件
        IconsResolver({
          prefix: 'Icon',
        }),
      ],

      dts: resolve(pathSrc, 'auto-imports.d.ts'),
    }),

    Components({
      resolvers: [
        // Auto register icon components
        // 自动注册图标组件
        IconsResolver({
          enabledCollections: ['ep'],
        }),
        // Auto register Element Plus components
        // 自动导入 Element Plus 组件
        ElementPlusResolver(),
      ],

      dts: resolve(pathSrc, 'components.d.ts'),
    }),

    Icons({
      autoInstall: true,
    }),
  ],
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@import "src/style/common.scss";',
      },
    },
  },
  preview: {
    open: true,
  },
});
