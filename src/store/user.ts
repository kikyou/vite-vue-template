import api from '@api/user';
import { defineStore } from 'pinia';

const useUserStore = defineStore('user', () => {
  let name = $ref('');
  let nickname = $ref('');
  let avatar = $ref('');
  let loading = $ref(false);
  const login = async (name: string) => {
    let data = await api.login({ name });
    if (data) {
      ({ nickname } = data);
      avatar = `${import.meta.env.VITE_API_URL}/img/${data.avatar}.webp`;
    }
  };
  return $$({ name, nickname, avatar, loading, login });
});
export default useUserStore;
