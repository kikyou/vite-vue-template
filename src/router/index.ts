const Home = () => import('../views/home.vue');
const routes = [{ path: '/', component: Home }];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
export default router;
