import api from './index';

export default {
  // 登录
  login(data: any) {
    return api.post<{
      nickname: string;
      avatar: string;
    }>('/login', data);
  },
};
