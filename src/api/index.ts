import type { AxiosInstance, AxiosRequestConfig } from 'axios';
import axios from 'axios';

interface IRespone<T> {
  data: T;
  msg: string;
  code: number;
}
class Api {
  instance: AxiosInstance;
  constructor(config: AxiosRequestConfig) {
    this.instance = axios.create(config);

    // 添加请求拦截器
    this.instance.interceptors.request.use(
      config =>
        // 在发送请求之前做些什么
        config,
      error =>
        // 对请求错误做些什么
        Promise.reject(error),
    );

    // 添加响应拦截器
    this.instance.interceptors.response.use(
      response =>
        // 2xx 范围内的状态码都会触发该函数。
        // 对响应数据做点什么
        response,
      error =>
        // 超出 2xx 范围的状态码都会触发该函数。
        // 对响应错误做点什么
        Promise.reject(error),
    );
  }
  request<T>(config: AxiosRequestConfig): Promise<T> {
    return new Promise<T>((res, rej) => {
      this.instance
        .request<T>(config)
        .then(data => {
          res(data.data);
        })
        .catch(err => {
          rej(err);
        });
    });
  }
  get<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return new Promise<T>((res, rej) => {
      this.instance
        .get<IRespone<T>>(url, config)
        .then(data => {
          res(data.data.data);
        })
        .catch(err => {
          rej(err);
        });
    });
  }
  post<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return new Promise<T>((res, rej) => {
      this.instance
        .post<IRespone<T>>(url, config)
        .then(data => {
          res(data.data.data);
        })
        .catch(err => {
          rej(err);
        });
    });
  }
}
const api = new Api({
  baseURL: import.meta.env.VITE_API_URL,
  timeout: 1000,
});
export default api;
