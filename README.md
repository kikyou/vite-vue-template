# Vite Vue3 最小化模板

> 开箱即用的轻量化模板

- [x] vite + vue3
- [x] typescript
- [x] vue-router
- [x] pinia
- [x] element-plus 自动引入组件 + 样式
- [x] lodash-es
- [x] axios + ts 封装
- [x] sass
- [x] ref / setup 最新语法糖
- [x] gitlab CI/CD 
- [ ] 更多功能开发中....

## 运行

```
pnpm i
pnpm dev
```
